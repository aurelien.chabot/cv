const puppeteer = require('puppeteer');
(async () => {
    const browser = await puppeteer.launch({
      executablePath: '/usr/bin/chromium-browser',
      args: ['--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'],
      headless: 'true'
    });

    const page = await browser.newPage()

    await page.goto('file://' + __dirname + '/www/cv.html', {waitUntil: 'networkidle0'});
    await page.pdf({path: 'www/Aurelien_Chabot_cv.pdf', format: 'a4'})

    await page.goto('file://' + __dirname + '/www/cv.fr.html', {waitUntil: 'networkidle0'});
    await page.pdf({path: 'www/Chabot_Aurelien_cv.pdf', format: 'a4'})

    await browser.close()
})()
