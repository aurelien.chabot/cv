#!/bin/sh

SOURCE="$( cd "$( dirname "$0" )" && pwd )"

if `which greadlink &> /dev/null`; then
    SOURCE_ROOT="$(greadlink -f $SOURCE)"
else
    SOURCE_ROOT="$(readlink -f $SOURCE)"
fi

# Interactive script?
if [ -t 1 ]; then
    OPT="-i"
fi

run() {
    image=$1
    shift
    docker run \
        -v $SOURCE_ROOT:$SOURCE_ROOT \
        -w $SOURCE_ROOT $OPT \
        $image sh -c "$@"
}

run "node:alpine" "apk add --update make && yarn add sass \
    && ln -s \`yarn bin sass\` /usr/local/bin/sass && make css"
run "python:alpine" "apk add --update make && pip3 install -r requirement.txt \
        && touch www/assets/css/cv.css && make web"
run "node:alpine" "apk add --update make chromium \
    && PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true yarn add puppeteer && make pdf"
run "python:alpine" "apk add --update make openjdk8-jre \
    && pip3 install html5validator \
    && html5validator --also-check-css --root www"
